import json
from unittest import mock


def test_delete(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_data(
        "{}"
    )
    url = httpserver.url_for("/foobar")

    with mock.patch(
        "sys.argv", ["idem", "exec", "http.json.delete", url, "--output=json"]
    ):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert {} == json.loads(captured.out)


def test_get(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_data("{}")
    url = httpserver.url_for("/foobar")

    with mock.patch(
        "sys.argv", ["idem", "exec", "http.json.get", url, "--output=json"]
    ):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert {} == json.loads(captured.out)


def test_head(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="HEAD").respond_with_data("{}")
    url = httpserver.url_for("/foobar")

    with mock.patch(
        "sys.argv", ["idem", "exec", "http.json.head", url, "--output=json"]
    ):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    ret = json.loads(captured.out)
    assert "Content-Length" in ret
    assert "Content-Type" in ret
    assert "Date" in ret
    assert "Server" in ret


def test_patch(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_data("{}")
    url = httpserver.url_for("/foobar")

    with mock.patch(
        "sys.argv", ["idem", "exec", "http.json.patch", url, "--output=json"]
    ):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert {} == json.loads(captured.out)


def test_post(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_data("{}")
    url = httpserver.url_for("/foobar")

    with mock.patch(
        "sys.argv", ["idem", "exec", "http.json.post", url, "--output=json"]
    ):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert {} == json.loads(captured.out)


def test_put(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_data("{}")
    url = httpserver.url_for("/foobar")

    with mock.patch(
        "sys.argv", ["idem", "exec", "http.json.put", url, "--output=json"]
    ):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert {} == json.loads(captured.out)
