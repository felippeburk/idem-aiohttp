from unittest import mock


def test_delete(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    with mock.patch("sys.argv", ["idem", "exec", "http.request.delete", url]):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert "foobar" in captured.out


def test_get(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    with mock.patch("sys.argv", ["idem", "exec", "http.request.get", url]):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert "foobar" in captured.out


def test_patch(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    with mock.patch("sys.argv", ["idem", "exec", "http.request.patch", url]):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert "foobar" in captured.out


def test_post(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    with mock.patch("sys.argv", ["idem", "exec", "http.request.post", url]):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert "foobar" in captured.out


def test_put(hub, httpserver, capsys):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_data(
        "foobar"
    )
    url = httpserver.url_for("/foobar")

    with mock.patch("sys.argv", ["idem", "exec", "http.request.put", url]):
        hub.idem.init.cli()

    captured = capsys.readouterr()
    assert "foobar" in captured.out
