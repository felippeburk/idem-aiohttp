import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    hub.pop.config.load(["idem", "acct"], "idem", parse_cli=False)

    yield hub


@pytest.fixture(scope="session")
def acct_subs():
    return ["http"]


@pytest.fixture(scope="session")
def acct_profile():
    return "test_idem_aiohttp"
