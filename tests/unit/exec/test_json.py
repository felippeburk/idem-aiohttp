import pytest


@pytest.mark.asyncio
async def test_delete(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_json(
        {"foo": "bar"}
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.json.delete(ctx, url)

    # check that the request is served
    assert ret == {"foo": "bar"}


@pytest.mark.asyncio
async def test_get(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_json(
        {"foo": "bar"}
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.json.get(ctx, url)

    # check that the request is served
    assert ret == {"foo": "bar"}


@pytest.mark.asyncio
async def test_head(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="HEAD").respond_with_json({})

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.json.head(ctx, url)

    # check that the request is served
    assert ret["Content-Length"] == "2"
    assert ret["Content-Type"] == "application/json"
    assert ret["Date"]
    assert ret["Server"]


@pytest.mark.asyncio
async def test_patch(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_json(
        {"foo": "bar"}
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.json.patch(ctx, url)

    # check that the request is served
    assert ret == {"foo": "bar"}


@pytest.mark.asyncio
async def test_post(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_json(
        {"foo": "bar"}
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.json.post(ctx, url)

    # check that the request is served
    assert ret == {"foo": "bar"}


@pytest.mark.asyncio
async def test_put(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_json(
        {"foo": "bar"}
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.json.put(ctx, url)

    # check that the request is served
    assert ret == {"foo": "bar"}
