import pytest


@pytest.mark.asyncio
async def test_delete(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="DELETE").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.request.delete(ctx, url)

    # check that the request is served
    assert ret == "foobar"


@pytest.mark.asyncio
async def test_get(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="GET").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.request.get(ctx, url)

    # check that the request is served
    assert ret == "foobar"


@pytest.mark.asyncio
async def test_patch(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PATCH").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.request.patch(ctx, url)

    # check that the request is served
    assert ret == "foobar"


@pytest.mark.asyncio
async def test_post(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="POST").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.request.post(ctx, url)

    # check that the request is served
    assert ret == "foobar"


@pytest.mark.asyncio
async def test_put(hub, ctx, httpserver):
    # set up the httpserver to serve /foobar with the json
    httpserver.expect_oneshot_request("/foobar", method="PUT").respond_with_data(
        "foobar"
    )

    url = httpserver.url_for("/foobar")
    ret = await hub.exec.http.request.put(ctx, url)

    # check that the request is served
    assert ret == "foobar"
