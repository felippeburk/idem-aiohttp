from unittest import mock

import pytest


@pytest.fixture(scope="session")
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    yield hub


@pytest.fixture(scope="session")
def acct_subs():
    return ["http"]


@pytest.fixture(scope="session")
def acct_profile():
    return "test_idem_aiohttp"
