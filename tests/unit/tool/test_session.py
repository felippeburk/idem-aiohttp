from unittest import mock

import pytest


@pytest.mark.asyncio
async def test_delete(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.http.session, "delete")


@pytest.mark.asyncio
async def test_get(hub, ctx, httpserver):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.http.session, "get")


@pytest.mark.asyncio
async def test_head(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.http.session, "get")


@pytest.mark.asyncio
async def test_patch(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.http.session, "patch")


@pytest.mark.asyncio
async def test_post(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.http.session, "post")


@pytest.mark.asyncio
async def test_put(hub):
    # Verify that the func alias made it onto the hub
    assert hasattr(hub.tool.http.session, "put")


@pytest.mark.asyncio
async def test_request_delete(hub):
    ...


@pytest.mark.asyncio
async def test_request_get(hub):
    ...


@pytest.mark.asyncio
async def test_request_head(hub):
    ...


@pytest.mark.asyncio
async def test_request_patch(hub):
    ...


@pytest.mark.asyncio
async def test_request_post(hub):
    ...


@pytest.mark.asyncio
async def test_request_put(hub):
    ...


@pytest.mark.skipif(not hasattr(mock, "AsyncMock"), reason="AsyncMock library missing")
@pytest.mark.asyncio
async def test_build_response(hub):
    resp = mock.MagicMock()
    resp.read = mock.AsyncMock()
    ret = await hub.tool.http.session.build_response(resp)
    resp.read.assert_called_once_with()
    resp.get_encoding.assert_called_once_with()
    assert ret.url == resp.url
    assert ret.status_code == resp.status
    assert ret.request == resp.request_info
    assert ret.headers == {}
    assert ret.history == []


def test_client(hub, ctx):
    mock_client = mock.MagicMock()
    connector = hub.tool.http.session.connector(ctx)

    hub.tool.http.session.client(ctx, mock_client)

    mock_client.assert_called_once_with(
        auth=None,
        loop=hub.pop.Loop,
        connector=connector,
        headers={},
        cookie_jar=hub.tool.http.session.COOKIES,
    )


def test_resolver(hub, ctx):
    mock_resolve = mock.MagicMock()
    ret = hub.tool.http.session.resolver(ctx, mock_resolve)
    assert ret

    # mock_resolve.assert_called_once_with()


def test_connector(hub, ctx):
    mock_connector = mock.MagicMock()
    ret = hub.tool.http.session.connector(ctx, mock_connector)
    assert ret

    # mock_connector.assert_called_once_with()
